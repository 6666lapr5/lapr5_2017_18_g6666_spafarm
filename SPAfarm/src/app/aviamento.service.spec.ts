import { TestBed, inject } from '@angular/core/testing';

import { AviamentoService } from './aviamento.service';

describe('AviamentoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AviamentoService]
    });
  });

  it('should be created', inject([AviamentoService], (service: AviamentoService) => {
    expect(service).toBeTruthy();
  }));
});
