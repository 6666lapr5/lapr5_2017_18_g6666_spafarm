import { Injectable } from '@angular/core';
import { Receita } from '../app/models/receita';
import { Prescricao } from '../app/models/prescricao';
import { Aviamento } from '../app/models/aviamento';
import { Observable } from 'rxjs/Rx';
import { of } from 'rxjs/observable/of';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthenticationService } from './authentication.service';
import { MessageService } from './message.service';
import { catchError, map, tap } from 'rxjs/operators';


@Injectable()
export class PrescricaoService {

  constructor(private http: HttpClient,
    private authenticationService: AuthenticationService,
    private messageService: MessageService) { }

  getPrescricoesById(id_p: String, id_r: string): Observable<Prescricao> {
    return this.http.get<Prescricao>(`https://gdflapr6666.azurewebsites.net/api/receita/${id_r}/prescricao/${id_p}`,
      this.getHeaders());
  }

  //PUT aviar a prescricao
  aviarPrescricao(id_r: string, id_p: string, quantidade: number): Observable<Aviamento> {
    return this.http.put<Aviamento>(`https://gdflapr6666.azurewebsites.net/api/receita/${id_r}/prescricao/${id_p}/aviar`,
      { nAviamentos: quantidade },
      this.getHeaders());
  }

  getHeaders() {
    let headers = new HttpHeaders({
      'x-access-token':
      this.authenticationService.userInfo.token
    });

    let httpOptions = {
      headers: headers
    };
    return httpOptions;
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** Log a PrescricaoService message with the MessageService */
  private log(message: string) {
    this.messageService.add('PrescricaoService: ' + message);
  }
}