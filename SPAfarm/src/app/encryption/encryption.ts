/// <reference types="crypto-js" />
import * as CryptoJS from 'crypto-js';

export class Encryption {

    key = ')DIE/z@q0>#n8ux?9k.eFZ%?Y.R~}_';
    iv = ')DIE/z@q0>#n8ux?9k.eFZ%?Y.R~}_';
    encrypt(unencrypted){
        var encrypted = CryptoJS.AES.encrypt(unencrypted, this.key,
            {
                keySize: 256 / 32,
                iv: this.iv,
                mode: CryptoJS.mode.CBC,
                padding: CryptoJS.pad.Pkcs7
            });
            return encrypted;
            
    }
    decrypt(encrypted){
        var decrypted = CryptoJS.AES.decrypt(encrypted, this.key, {
            keySize: 256 / 32,
            iv: this.iv,
            mode: CryptoJS.mode.CBC,
            padding: CryptoJS.pad.Pkcs7
        });
        return decrypted.toString(CryptoJS.enc.Utf8);
    }
}