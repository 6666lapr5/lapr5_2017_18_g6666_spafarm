import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../authentication.service';
import { Aviamento } from '../models/aviamento';
import { AviamentoService } from '../aviamento.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: [ './dashboard.component.css' ]
})
export class DashboardComponent implements OnInit {
  aviamentos: Aviamento[];
  farmacia: String;

  constructor(
    private authenticationService: AuthenticationService,
    private aviamentoService: AviamentoService) {}

  ngOnInit() {
    this.aviamentos = this.aviamentoService.listaAviamentos;
    this.farmacia = this.authenticationService.userInfo.farmacia;
  }

}
