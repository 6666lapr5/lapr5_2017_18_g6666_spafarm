import { Component, OnInit, Input } from '@angular/core';
import { Receita } from '../models/receita';
import { ReceitasService } from '../receitas.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { PrescricaoService } from '../prescricao.service';
import { Prescricao } from '../models/prescricao';

@Component({
  selector: 'app-receitas-farmaceutico',
  templateUrl: './receitas-farmaceutico.component.html',
  styleUrls: ['./receitas-farmaceutico.component.css']
})
export class ReceitasFarmaceuticoComponent {
  
  receita: Receita;
  prescricao: Prescricao;
  model: any = {};

  constructor(private router: Router,
    private activatedRoute: ActivatedRoute,
    private location: Location,
    private receitasService: ReceitasService,
    private prescricaoService: PrescricaoService) { }

  searchReceitas(id: string): void {
    this.receitasService.getReceitaByID(id)
    .subscribe(receita =>
      this.receita = receita
    );
  }
  
  getPrescricao(presc_id: string, receita_id: string){
    this.prescricaoService.getPrescricoesById(presc_id, receita_id)
    .subscribe(prescricao => {
      this.prescricao = prescricao;
    })
  }

  aviar(idPrescricao: String, idReceita: String){
    this.router.navigate(['/receita/'+idReceita+"/prescricao/"+idPrescricao+"/aviar"]);
  }

}
