import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReceitasFarmaceuticoComponent } from './receitas-farmaceutico.component';

describe('ReceitasFarmaceuticoComponent', () => {
  let component: ReceitasFarmaceuticoComponent;
  let fixture: ComponentFixture<ReceitasFarmaceuticoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReceitasFarmaceuticoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReceitasFarmaceuticoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
