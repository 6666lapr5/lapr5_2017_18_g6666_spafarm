import { Component, ChangeDetectorRef } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { AuthenticationService } from './authentication.service';
import { User } from './models/user';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { StocksService } from './stocks.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'PIMMédica Farmacêuticos';
  subscriptionAuth: Subscription;
  userInfo: User;
  constructor(
    private authenticationService: AuthenticationService,
    private stocksService: StocksService,
    private cdr: ChangeDetectorRef) { }
  ngOnInit() {
    this.userInfo = this.authenticationService.userInfo;
    this.subscriptionAuth =
      this.authenticationService.authentication.subscribe((userInfo) => {
        this.userInfo = userInfo;
        this.cdr.detectChanges();
      });
  }
  restock(){
    this.stocksService.restock(this.userInfo.farmacia).subscribe(result => {
    });
  }

  ngOnDestroy() {
    this.subscriptionAuth.unsubscribe();
  }
}
