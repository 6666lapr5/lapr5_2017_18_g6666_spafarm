import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Aviamento } from '../app/models/aviamento';

@Injectable()
export class AviamentoService {

  public listaAviamentos: Aviamento[] = [];

  constructor() { }

  addAviamento(aviamento: Aviamento) {
    this.listaAviamentos.push(aviamento);
  }
}
