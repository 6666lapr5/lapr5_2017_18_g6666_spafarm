import { Component, OnInit } from '@angular/core';
import { Apresentacao } from '../models/apresentacao';
import { ApresentacaoMedsService } from '../apresentacao-meds.service';
import { Observable } from 'rxjs/Rx';
import { of } from 'rxjs/observable/of';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-apresentacao-meds',
  templateUrl: './apresentacao-meds.component.html',
  styleUrls: ['./apresentacao-meds.component.css']
})
export class ApresentacaoMedsComponent implements OnInit {

  apresentacoes: Apresentacao[] = [];

  constructor(private apresentacoesService: ApresentacaoMedsService) { }

  ngOnInit() { }

  getApresentacoes(nome: string) {
    this.apresentacoesService.getApresentacoes(nome)
      .subscribe(apresentacoes => {
        this.apresentacoes = apresentacoes;
      })
  }

}