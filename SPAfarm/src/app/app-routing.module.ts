import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DashboardComponent }   from './dashboard/dashboard.component';

import { LoginComponent } from './login/login.component';
import { AuthGuard } from './guards/auth.guard';
import { MedicoGuard } from './guards/medico.guard';
import { UtenteGuard } from './guards/utente.guard';
import { ReceitasFarmaceuticoComponent } from './receitas-farmaceutico/receitas-farmaceutico.component';
import { FarmaceuticoGuard } from './guards/farmaceutico.guard';
import { AviamentosComponent } from './aviamentos/aviamentos.component';
import { ApresentacaoMedsComponent } from './apresentacao-meds/apresentacao-meds.component';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'receitas', component: ReceitasFarmaceuticoComponent, canActivate: [AuthGuard, FarmaceuticoGuard] },
  { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard, FarmaceuticoGuard] },
  { path: 'receita/:id1/prescricao/:id2/aviar', component: AviamentosComponent, canActivate: [AuthGuard, FarmaceuticoGuard] },
  { path: 'apresentacoes', component: ApresentacaoMedsComponent},
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
