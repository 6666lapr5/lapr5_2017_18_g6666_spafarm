import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
//import { InMemoryDataService }  from './in-memory-data.service';

import { AppRoutingModule } from './app-routing.module';

import { AuthGuard } from './guards/auth.guard';
import { FarmaceuticoGuard } from './guards/farmaceutico.guard';

import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
import { ReceitasFarmaceuticoComponent } from './receitas-farmaceutico/receitas-farmaceutico.component';
import { AviamentosComponent } from './aviamentos/aviamentos.component';
import { ApresentacaoMedsComponent } from './apresentacao-meds/apresentacao-meds.component';
import { MessagesComponent } from './messages/messages.component';

import { ReceitasService } from './receitas.service';
import { AuthenticationService } from './authentication.service';
import { PrescricaoService } from './prescricao.service';
import { ApresentacaoMedsService } from './apresentacao-meds.service';
import { MessageService } from './message.service';
import { AviamentoService } from './aviamento.service';
import { Encryption } from './encryption/encryption';
import {StocksService} from './stocks.service';


@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,

    // The HttpClientInMemoryWebApiModule module intercepts HTTP requests
    // and returns simulated server responses.
    // Remove it when a real server is ready to receive requests.
    /*HttpClientInMemoryWebApiModule.forRoot(
      InMemoryDataService, { dataEncapsulation: false }
    )*/
  ],
  declarations: [
    AppComponent,
    DashboardComponent,
    LoginComponent,
    MessagesComponent,
    ReceitasFarmaceuticoComponent,
    AviamentosComponent,
    ApresentacaoMedsComponent
  ],
  providers: [AuthGuard,
    FarmaceuticoGuard,
    AuthenticationService,
    MessageService,
    ReceitasService,
    PrescricaoService,
    ApresentacaoMedsService,
    AviamentoService,
    StocksService,
    Encryption
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
