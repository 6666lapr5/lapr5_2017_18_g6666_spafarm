import { Component, OnInit, Input } from '@angular/core';
import { Prescricao } from '../models/prescricao';
import { PrescricaoService } from '../prescricao.service';
import { ReceitasService } from '../receitas.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Receita } from '../models/receita';
import { Aviamento } from '../models/aviamento';
import { AviamentoService } from '../aviamento.service';

@Component({
  selector: 'app-aviamentos',
  templateUrl: './aviamentos.component.html',
  styleUrls: ['./aviamentos.component.css']
})
export class AviamentosComponent implements OnInit {

  model: any = {};
  prescricao: Prescricao;
  idRec: string;
  idPres: string;
  receita: Receita;
  aviamento: Aviamento;

  constructor(
    private route: ActivatedRoute,
    private receitasService: ReceitasService,
    private prescricaoService: PrescricaoService,
    private aviamentoService: AviamentoService) { }

  ngOnInit(): void {
    this.idRec = this.route.snapshot.paramMap.get('id1');
    this.idPres = this.route.snapshot.paramMap.get('id2');
    this.receitasService.getReceitaByID(this.idRec)
      .subscribe(receita => this.receita = receita);
    this.prescricaoService.getPrescricoesById(this.idPres, this.idRec)
      .subscribe(prescricao => this.prescricao = prescricao);
  }

  aviarPrescricao(quantidade: number) {
    this.prescricaoService.aviarPrescricao(this.receita.id, this.prescricao.id, quantidade)
      .subscribe(aviamento => {
        this.aviamento = aviamento;

        if(aviamento) {
          alert('Prescrição aviada!');
          this.aviamentoService.addAviamento(aviamento);
        }
      })
  }
}
