import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AviamentosComponent } from './aviamentos.component';

describe('AviamentosComponent', () => {
  let component: AviamentosComponent;
  let fixture: ComponentFixture<AviamentosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AviamentosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AviamentosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
