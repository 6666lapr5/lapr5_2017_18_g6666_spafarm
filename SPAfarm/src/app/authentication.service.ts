import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from
  '@angular/common/http';
import { Subject } from 'rxjs';
import { Observable } from 'rxjs/Observable';
import * as jwt_decode from 'jwt-decode';
import { User } from './models/user';
import { Encryption } from './encryption/encryption';

class Token { token: string };

@Injectable()
export class AuthenticationService {
  private authUrl = 'https://gdflapr6666.azurewebsites.net/api/authenticate';
  //verificar o servidor:porta
  public userInfo: User;
  authentication: Subject<User> = new Subject<User>();
  constructor(
    private http: HttpClient,
    private encryption: Encryption
  ) {
    this.userInfo = localStorage.userInfo;
  }
  login(email: string, password: string): Observable<boolean> {
    return new Observable<boolean>(observer => {
      var encEmail:string;
      encEmail = this.encryption.encrypt(email).toString();
      var encPassword:string;
      encPassword = this.encryption.encrypt(password).toString();
      this.http.post<Token>(this.authUrl, {
        email: encEmail,
        password: encPassword
      })
        .subscribe(data => {
          var token = data.token;
          if (token) {
            const tokenDecoded = jwt_decode(token);
            this.userInfo = {
              token: token,
              tokenExp: tokenDecoded.exp,
              medico: tokenDecoded.medico,
              farmaceutico: tokenDecoded.farmaceutico,
              utente: tokenDecoded.utente,
              farmacia : tokenDecoded.farmacia
            }
            localStorage.userInfo = this.userInfo;

            this.authentication.next(this.userInfo);
            observer.next(true);
          } else {
            this.authentication.next(this.userInfo);
            observer.next(false);
          }
        },
        (err: HttpErrorResponse) => {
          if (err.error instanceof Error) {
            console.log("Client-side error occured.");
          } else {
            console.log("Server-side error occured.");
          }
          console.log(err);
          this.authentication.next(this.userInfo);
          observer.next(false);
        });

    });
  }
  logout() {
    this.userInfo = null;
    localStorage.removeItem('userInfo');
    this.authentication.next(this.userInfo);
  }
}
