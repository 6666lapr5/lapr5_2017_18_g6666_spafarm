import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthenticationService } from './authentication.service';
import { Observable } from 'rxjs/Rx';
import { Stock } from '../app/models/stock';
import { MessageService } from './message.service';
import { catchError, map, tap } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';


@Injectable()
export class StocksService {

  constructor(private http: HttpClient,
    private authenticationService: AuthenticationService, private messageService: MessageService) { }

  restock(farmacia: string) : Observable<boolean> {
    var url = 'http://localhost:8443/api/stock/restockManual/'+farmacia;
    return this.http.put<boolean>(url,{});
  }

  /**
 * Handle Http operation that failed.
 * Let the app continue.
 * @param operation - name of the operation that failed
 * @param result - optional value to return as the observable result
 */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** Log a PrescricaoService message with the MessageService */
  private log(message: string) {
    this.messageService.add('PrescricaoService: ' + message);
  }
}
