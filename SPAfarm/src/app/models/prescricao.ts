export class Prescricao {
    id: string;
    apresentacao: string;
    validade: Date;
    posologiaPrescrita: {
        quantidade: number;
        dias: number;
        intervalo_horas: number;
    };
    aviamentos: Date[];
}