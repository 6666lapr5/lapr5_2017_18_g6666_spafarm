export class Apresentacao {
    id: number;
    formaFarmaceutica: string;
	dosagem: string;
	tamanhoEmbalagem: number;
}