export class Aviamento {
    idPrescricao: Number;
    apresentacao: string;
    dataAviamento: Date;
    farmaceutico: string;
    nAviamentos: Number
}