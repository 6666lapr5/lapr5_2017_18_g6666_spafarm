import { Prescricao } from "./prescricao";

export class Receita {
    id: string;
    utente: string;
    medico: string;
    data: Date;
    prescricoes: Prescricao[];
}